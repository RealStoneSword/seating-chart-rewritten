#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TABLE_SIZE_X 10
#define TABLE_SIZE_Y 10
#define TABLE_TEXT_SIZE 8
#define PROMPT "# "

char *table[TABLE_SIZE_X][TABLE_SIZE_Y];

void set_cell(char input[64]){
        char *cell_x_tmp, *cell_y_tmp, *cell_val;
        int cell_x, cell_y;
        strtok(input, " ");
        cell_x_tmp = strtok(NULL, " ");
        cell_y_tmp = strtok(NULL, " ");
        cell_val = strtok(NULL, " ");
        if(!cell_x_tmp || !cell_y_tmp || !cell_val) {
            printf("Not enough arguments.\n");
            return;
        }
        cell_y = atoi(cell_y_tmp);
        cell_x = atoi(cell_x_tmp);
        cell_val[strlen(cell_val)-1] = '\0';
        if(strlen(cell_val) > TABLE_TEXT_SIZE){
            printf("String too long!\n");
            return;
        }
        printf("%s", cell_val);
        table[cell_x][cell_y] = cell_val;
}

void print_table() {
    printf("Finish this you lazy bum\n");
}

int main(){
    printf("Type cmds for commands, type help for help/info");
    while(1) {
        char input[64];
        printf("%s", PROMPT);
        fgets(input, 64, stdin);
        if(strncmp(input, "cmds", 4) == 0) {
            puts(
                "Commands:\n"
                "cmds - Displays this list\n"
                "help - Displays some help and info\n"
                "print - Prints the table\n"
                "cell (cellx) (celly) (cellval) - Sets the value of a cell\n"
                "reset - Resets the table back to nothing\n"
                "exit - Exits the program"
                 );
        }
        else if (strncmp(input, "help", 4) == 0) {
            puts(
                "seating-chart-rewritten\n"
                "Made by Ethan F (RealStoneSword)\n"
                "Type cmds for commands"
            );
        }
        else if (strncmp(input, "print", 5) == 0) {
            print_table();
        }
        else if (strncmp(input, "cell", 4) == 0) {
            set_cell(input);
        }
    }
}