#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#define XTABLE 5 /* how many cells long the table is */
#define YTABLE 6 /* how many cells tall the table is */
#define TABLECHAR 7 /* amount of characters per cell */

char *table[XTABLE][YTABLE];

void printtable() {
    for(int x = 0; x < YTABLE; x++) {
        for(int i = 0; i < XTABLE * TABLECHAR + XTABLE + 1; i++)
            putchar('-');
        printf("\n");
        for(int z = 0; z < XTABLE; z++)
            printf("|%*s", TABLECHAR, table[x][z]);
        printf("|\n");
    }
        for(int i = 0; i < XTABLE * TABLECHAR + XTABLE + 1; i++)
            putchar('-');
}

void prompt() {
    char input[255];
    printf("> ");
    fgets(input, 255, stdin);
    if(strncmp(input, "help", 1) == 0) {
        puts(
                "Commands:\n"
                "help - Displays this list\n"
                "print - Prints the table\n"
                "cell (cellx) (celly) (cellval) - Sets the value of a cell\n"
                "exit - Exits the program"
            );
    }
    else if(strncmp(input, "print", 5) == 0) {
        printtable();
        printf("\n");
    }
    else if(strncmp(input, "cell", 4) == 0) {
        char *cell_x_tmp, *cell_y_tmp, *cell_val;
        int cell_x, cell_y;
        strtok(input, " ");
        cell_x_tmp = strtok(NULL, " ");
        cell_y_tmp = strtok(NULL, " ");
        cell_val = strtok(NULL, " ");
        if(!cell_x_tmp || !cell_y_tmp || !cell_val) {
            printf("Not enough arguments.\n");
            return;
        }
        cell_y = atoi(cell_y_tmp);
        cell_x = atoi(cell_x_tmp);
        cell_val[strlen(cell_val)-1] = '\0';
        if(strlen(cell_val) > TABLECHAR){
            printf("String too long!\n");
            return;
        }
        printf("%s", cell_val);
        table[cell_x][cell_y] = cell_val;
    }
    else if(strncmp(input, "exit", 4) == 0) {
        exit(0);
    }
    else {
        printf("Unknown command.\n");
    }
}

int main() {
    for(int f = 0; f < XTABLE + 1; f++) {
        for(int d = 0; d < YTABLE + 1; d++) { 
            table[f][d] = " ";
        }
    }
    printtable();
    printf("\nType help for help\n");
    while(1) {
    prompt();
    }
    return 0;
}




